package java_code;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;


public class WriterClass {
    
    
    public WriterClass(String path, String text){
        File myFile = new File(path);
        if(myFile.exists()){

            try{
                FileWriter file_writer = new FileWriter(myFile,true);
                BufferedWriter buffered_writer = new BufferedWriter (file_writer);
                buffered_writer.write(text);
                buffered_writer.close();
                System.out.println("\"Run\" to append string to newFile2!");
            }
            catch(Exception exc){
                exc.printStackTrace();
            }
        }
            else{
                    try{
                        System.out.println("newFile2 Created.");
                        FileWriter file_writer = new FileWriter (myFile);
                        BufferedWriter buffered_writer = new BufferedWriter(file_writer);
                        buffered_writer.write(text);
                        buffered_writer.close();
                        System.out.println("appending string to newFile2");
                        }
                    catch(Exception exc){
                        exc.printStackTrace();
                    }
                    
                    try{
                        myFile.createNewFile();
                    }
                    catch(Exception exc){
                        exc.printStackTrace();
                    }
                }
           
        
    }

}
