package java_code;

public class Java_code {

    
    public static void main(String[] args) throws Exception {
       
        ReaderClass reader = new ReaderClass("C:/Users/User/Desktop/newFile1.txt"); //reading newFile1 file 
        WriterClass writer = new WriterClass("C:/Users/User/Desktop/newFile2.txt",reader.toString()); //writing newFile2 file

    }
    
}

/*
TEST PENTASTAGIU;
Write a Java program that reads a text from a file and then writes it into another file.
The following elements will provide a higher score:
OOP approach, meaning you have at least one more classes besides the main class.
Java 8 functionality for writing file contents.
Correct try-catch exception management implemented.
Your application is available on Bitbucket or any other publicly accessible Git repository.
*/

/**
 * AUTHOR: CIOCOI S. CRISTIAN
 * UNIVERSITY: Universitatea "Dunărea de Jos" din Galați 
 * 08/03/2019
 */