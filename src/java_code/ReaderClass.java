package java_code;

import java.io.BufferedReader;
import java.io.FileReader;

public class ReaderClass {
    private String text = "";
    
    public ReaderClass(String path) throws Exception{
        FileReader file = new FileReader(path);
        BufferedReader read = new BufferedReader(file);
        String line = read.readLine();
        
        while (line != null){
            text += line + "\n";
            line = read.readLine();
        }
        read.close();
    }
    public String toString(){
        return text;
    }
}
